FROM node:10.14 as group-bells
COPY ./ /app
WORKDIR /app
RUN yarn install &&  \
    apt-get update &&  \
    apt-get -y install gettext-base jq &&  \
    VERSION=$(jq -r .version package.json) &&  \
    envsubst < src/util/Version.ts > tmp.ts && \
    mv tmp.ts src/util/Version.ts && \
    cat src/util/Version.ts &&  \
    node_modules/webpack/bin/webpack.js


# -------------------- S T A G E   F I N A L ------------------
FROM trafex/php-nginx
COPY --chown=nginx --from=group-bells /app/demo /var/www/html
RUN rm /var/www/html/dist && rm /var/www/html/lists
COPY --chown=nginx --from=group-bells /app/dist/ /var/www/html/dist/
COPY --chown=nginx --from=group-bells /app/lists/ /var/www/html/lists/

