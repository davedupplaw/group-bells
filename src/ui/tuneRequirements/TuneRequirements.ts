import * as Riot from 'riot-typescript';

import * as bellSVG from '../../assets/notification-bell.svg';
import * as template from './TuneRequirements.html';
import './tuneRequirements.scss';

import {Tune} from "../../score/Tune";
import {Note} from "../../score/Note";
import PubSub from "../../util/PubSub";

@Riot.template(template.toString())
export default class TuneRequirements extends Riot.Element {
    private title : string;
    private key: string;
    private bellRequirements : object = {};
    private bellSVG: string;

    constructor() {
        super();

        this.bellSVG = bellSVG.toString();
    }
    
    mounted() {
        PubSub.subscribe( 'tuneChosen', (tune) => this.showRequirements(tune) );
        this.root.setAttribute( 'style', 'display: none;' );
    }

    close() {
        this.root.setAttribute( 'style', 'display: none;' );
    }

    private clearTuneRequirements() {
        let bellList: HTMLElement = this.root.getElementsByClassName('bellList')[0] as HTMLElement;
        while (bellList.hasChildNodes()) {
            bellList.removeChild(bellList.lastChild);
        }
    }

    private showRequirements(tune: Tune) {
        this.title = tune.title;
        this.key = tune.key;

        this.clearTuneRequirements();
        this.updateRequirementMap(tune);

        let bellList: HTMLElement = this.root.getElementsByClassName('bellList')[0] as HTMLElement;
        for( let noteNumber in this.bellRequirements ) {
            if( this.bellRequirements.hasOwnProperty(noteNumber) ) {
                bellList.appendChild( this.createBellRequired( this.bellRequirements[noteNumber].note, this.bellRequirements[noteNumber].count ) );
            }
        }

        this.root.setAttribute( 'style', 'display: flex;' );
        this.update();
    }

    private updateRequirementMap(tune: Tune) {
        let bellRequirements = [];
        let bellCount = [];
        for (let note of tune.notes) {
            if (note.pitch) {
                bellRequirements[note.pitch.value] = note;

                if( !bellCount[note.pitch.value] ) {
                    bellCount[note.pitch.value] = 0;
                }
                bellCount[note.pitch.value]++;
            }
        }

        this.bellRequirements = [];
        for (let note of bellRequirements) {
            if (note) {
                this.bellRequirements[note.pitch.value] = {note: note, count: bellCount[note.pitch.value]};
            }
        }
    }

    private createBellRequired(note: Note, count: number): HTMLElement {
        let bellDiv = document.createElement('div' ) as HTMLDivElement;
        bellDiv.className = `bell colour${note.pitch.value}`;
        bellDiv.innerHTML = this.bellSVG;

        let requirementDiv = document.createElement('div') as HTMLDivElement;
        requirementDiv.className = 'requirement';
        requirementDiv.appendChild( bellDiv );
        requirementDiv.appendChild( document.createTextNode( `${note.pitch.name} (${count})` ) );

        return requirementDiv;
    }
}