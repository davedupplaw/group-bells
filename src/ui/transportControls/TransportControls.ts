import * as Riot from 'riot-typescript';
import * as template from './TransportControls.html';
import '@fortawesome/fontawesome'

import TimingController from "../../util/TimingController";
import PubSub from "../../util/PubSub";

import './transportControls.scss';

@Riot.template(template.toString())
export class TransportControls extends Riot.Element {
    private tempo: number = 100;

    private playDisabled = false;
    private pauseDisabled = false;
    private stopDisabled = true;

    constructor() {
        super();
    }

    mounted() {
        PubSub.subscribe('tuneChosen', (tune) => {
            this.tempo = tune.tempo;
            this.update();
        });
        PubSub.subscribe('toggleTransport', () => TimingController.toggle());

        PubSub.subscribe('transportStarted', () => {
            this.playDisabled = true;
            this.pauseDisabled = this.stopDisabled = false;
            this.update();
        });

        PubSub.subscribe('transportPaused', () => {
            this.playDisabled = this.stopDisabled = false;
            this.pauseDisabled = true;
            this.update();
        });

        PubSub.subscribe('transportReset', () => {
            this.stopDisabled = true;
            this.playDisabled = false;
            this.update();
        });
    }

    play() {
        console.log("Play pressed");
        TimingController.start();
    }

    pause() {
        console.log("Pause pressed");
        TimingController.pause();
    }

    stop() {
        console.log("Stop pressed");
        TimingController.pauseAndReset();
    }

    updateDisplay(e) {
        PubSub.publish('rendererChanged', parseInt(e.target.value));
    }

    updateTempo(e) {
        this.tempo = parseInt(e.target.value);
        TimingController.setTempo(this.tempo);
        PubSub.publish('tempoChanged', this.tempo);
    }
}