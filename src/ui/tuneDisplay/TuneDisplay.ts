import * as Riot from 'riot-typescript';

import * as template from './TuneDisplay.html';
import './tuneDisplay.scss';

import {Renderer} from "./renderers/Renderer";
import {NoteLetterRenderer} from "./renderers/NoteLetterRenderer";
import {BellRenderer} from "./renderers/BellRenderer";

import {Tune} from "../../score/Tune";
import DisplayConfiguration from "../../util/DisplayConfiguration";

import PubSub from "../../util/PubSub";
import TimingController from "../../util/TimingController";
import TuneRequirements from "../tuneRequirements/TuneRequirements";
import {BellRendererWithNotes} from "./renderers/BellRendererWithNotes";

@Riot.template(template.toString())
export class TuneDisplay extends Riot.Element {
    private renderers: Renderer[] = [ new NoteLetterRenderer(), new BellRenderer(), new BellRendererWithNotes() ];
    private currentRenderer: Renderer;
    private currentPositionPx: number = 0;
    private drawingElement: HTMLElement;
    private currentTune: Tune;
    private currentPosition: number;

    constructor() {
        super();

        // Avoid tree shaking
        new TuneRequirements();

        this.currentRenderer = this.renderers[2];
    }

    mounted() {
        PubSub.subscribe('tuneChosen', (data) => this.tuneChosen(data) );
        PubSub.subscribe( 'transportMoved', (positionVals) => this.setCurrentPosition(positionVals) );
        PubSub.subscribe( 'rendererChanged', (index: number) => this.updateRenderer(index) );
        this.drawingElement = this.root.getElementsByClassName('tuneDisplay')[0] as HTMLElement;
    }

    tuneChosen( tune: Tune ) {
        this.currentTune = tune;
        this.renderTune(tune);
        this.setCurrentPosition( 0 );
        TimingController.setTempo( tune.tempo );
    }

    private renderTune(tune: Tune) {
        this.clearDrawingElement();
        this.currentRenderer.render(tune, this.drawingElement);
    }

    private clearDrawingElement() {
        while (this.drawingElement.hasChildNodes()) {
            this.drawingElement.removeChild(this.drawingElement.lastChild);
        }
    }

    private setCurrentPosition( positionVals: number ) {
        this.currentPosition = positionVals;
        this.currentPositionPx = DisplayConfiguration.positionMarkerAt - (positionVals / (DisplayConfiguration.valuesPerBeat * DisplayConfiguration.beatsPerPixel));
        this.drawingElement.setAttribute('style', `transform: translate(${this.currentPositionPx}px,0)`);

        if( positionVals > this.currentTune.tuneLength ) {
            TimingController.pause();
        }
    }

    private updateRenderer(index: number) {
        this.currentRenderer = this.renderers[index];
        this.renderTune(this.currentTune);
    }
}