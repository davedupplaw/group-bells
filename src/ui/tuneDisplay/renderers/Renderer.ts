import {Tune} from "../../../score/Tune";

export interface Renderer {
    render(tune: Tune, element: HTMLElement);
}