import TimelineRenderer from "./TimelineRenderer";

import DisplayConfiguration from "../../../util/DisplayConfiguration";

import {Note} from "../../../score/Note";

export class NoteLetterRenderer extends TimelineRenderer {
    protected createNoteSpan( note: Note, currentPosition: number ) : HTMLElement {
        const topPosition = 400 - (note.pitch.value - 60) * 16;
        const noteLength = note.length.value / DisplayConfiguration.valuesPerBeat / DisplayConfiguration.beatsPerPixel;
        const noteSpan = document.createElement('span');
        noteSpan.appendChild( document.createTextNode(note.pitch.name) );
        noteSpan.className = `noteLetter colour${note.pitch.value}`;
        noteSpan.setAttribute('style', `left: ${currentPosition}px; top: ${topPosition}px; width: ${noteLength}px`);
        return noteSpan;
    }

    protected getClassName() {
        return 'noteLetterDisplay';
    }
}