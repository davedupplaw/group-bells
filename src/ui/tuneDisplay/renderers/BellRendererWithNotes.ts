import TimelineRenderer from "./TimelineRenderer";

import {Note} from "../../../score/Note"

import * as bellSVG from '../../../assets/notification-bell.svg';
import {BellRenderer} from "./BellRenderer";

export class BellRendererWithNotes extends BellRenderer {
    constructor() {
        super();
    }

    protected createNoteSpan( note: Note, currentPosition: number ) : HTMLElement {
        const bellDiv = super.createNoteSpan(note, currentPosition);
        const noteSpan = document.createElement('div');
        noteSpan.className = 'bellWithNote';
        noteSpan.appendChild( bellDiv );

        const nameDiv = document.createElement('div');
        nameDiv.appendChild(document.createTextNode(note.pitch.name));
        nameDiv.className = `bellName colour${note.pitch.value}`;
        noteSpan.appendChild( nameDiv );

        noteSpan.setAttribute('style', bellDiv.getAttribute('style') );
        bellDiv.removeAttribute('style');
        return noteSpan;
    }

    protected getClassName() {
        return 'bellDisplay';
    }
}