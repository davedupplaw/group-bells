import TimelineRenderer from "./TimelineRenderer";

import {Note} from "../../../score/Note"

import * as bellSVG from '../../../assets/notification-bell.svg';
import {Tune} from "../../../score/Tune";

export class BellRenderer extends TimelineRenderer {
    private bell: any;

    private drawLines = false;
    private drawTo = 70;
    private drawDownTo = 50;
    private yPositionOfMiddleC = 400;

    constructor() {
        super();
        this.bell = bellSVG;
    }

    render( tune: Tune, element: HTMLElement ) {
        if( this.drawLines ) {
            for( let i = this.drawDownTo; i < this.drawTo; i++ ) {
                const position = this.yPositionOfMiddleC - (i - 60) * 16;
                const lineSpan = document.createElement('div');
                lineSpan.className = `noteline noteline${i}`;
                lineSpan.setAttribute('style', `top: ${position}px`)

                element.appendChild( lineSpan );
            }
        }

        return super.render( tune, element );
    }

    protected createNoteSpan( note: Note, currentPosition: number ) : HTMLElement {
        const topPosition = this.yPositionOfMiddleC - (note.pitch.value - 60) * 16;
        const noteSpan = document.createElement('div');
        noteSpan.className = `bell colour${note.pitch.value}`;
        noteSpan.innerHTML = this.bell;
        noteSpan.setAttribute('style', `left: ${currentPosition}px; top: ${topPosition}px;`);
        return noteSpan;
    }

    protected getClassName() {
        return 'bellDisplay';
    }
}