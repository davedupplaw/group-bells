import {Renderer} from "./Renderer";
import {Tune} from "../../../score/Tune";
import DisplayConfiguration from "../../../util/DisplayConfiguration";
import {Note} from "../../../score/Note";

export default class TimelineRenderer implements Renderer {
    render( tune: Tune, element: HTMLElement ) {
        element.className = this.getClassName();

        let currentPosition = 0;
        for( let note of tune.notes ) {
            let musicalItemElement;

            if( note.isBarLine ) {
                musicalItemElement = TimelineRenderer.createBar(currentPosition, tune, element);
            } else if( note.isComment ) {
                musicalItemElement = TimelineRenderer.createCommentSpan(note, currentPosition);
            } else {
                if( !note.isRest ) {
                    musicalItemElement = this.createNoteSpan(note, currentPosition);
                }
            }

            if( musicalItemElement ) {
                element.appendChild( musicalItemElement );
            }

            currentPosition += (note.length.value / DisplayConfiguration.valuesPerBeat) / DisplayConfiguration.beatsPerPixel;
        }
    }

    // This gets overridden in the renderer subclasses
    protected createNoteSpan( note: Note, currentPosition: number ) : HTMLElement {
        return null;
    }

    private static createBar(currentPosition: number, tune: Tune, element: HTMLElement) {
        const barDiv = document.createElement('div');
        barDiv.className = `barLine`;
        barDiv.setAttribute('style', `left: ${currentPosition}px;`);

        TimelineRenderer.addBeatLinesForBar(tune, currentPosition, element);

        return barDiv;
    }

    private static addBeatLinesForBar(tune: Tune, currentPosition: number, element: HTMLElement) {
        for (let n = 0; n < tune.beatsPerBar; n++) {
            const beatDiv = document.createElement('div');
            beatDiv.className = `beatLine`;
            beatDiv.setAttribute('style', `left: ${currentPosition + n / DisplayConfiguration.beatsPerPixel}px;`);
            element.appendChild(beatDiv);
        }
    }

    protected getClassName() {
        return '';
    }

    private static createCommentSpan(note: Note, currentPosition: number) {
        const commentSpan = document.createElement('span');
        commentSpan.appendChild( document.createTextNode(note.comment) );
        commentSpan.className = 'comment';
        commentSpan.setAttribute('style', `left: ${currentPosition}px; top: 0px;`);
        return commentSpan;

    }
}