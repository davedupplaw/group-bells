import * as Riot from 'riot-typescript';

@Riot.template('<raw></raw>')
export default class Raw extends Riot.Element {
    private rawContent: string;

    constructor(opts) {
        super();
        this.rawContent = opts.rawContent;
    }

    updating() {
        this.root.innerHTML = this.rawContent;
    }
}