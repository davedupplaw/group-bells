import * as Riot from 'riot-typescript';

import {Version} from '../util/Version';
import PubSub from "../util/PubSub";

import * as template from './BellsApp.html';
import './default.scss';

// To avoid tree-shaking these
import {TuneList} from "./tuneList/TuneList";
import {TuneDisplay} from "./tuneDisplay/TuneDisplay";
import {TransportControls} from "./transportControls/TransportControls";
import {CollectionList} from "./collectionList/CollectionList";

@Riot.template(template.toString())
export class BellsApp extends Riot.Element {
    private version = new Version().version;

    constructor() {
        super();

        // Avoid tree-shaking
        new TuneList();
        new CollectionList();
        new TuneDisplay();
        new TransportControls();
    }

    mounted() {
        document.addEventListener('keydown', (event) => {
            if (event.keyCode == 32) {
                PubSub.publish('toggleTransport', {});
            }
        });
    }
}
