import * as Riot from 'riot-typescript';
import PubSub from "../../util/PubSub";
import {StorageService} from "../../util/StorageService";
import {Storage} from "../../util/Storage";

import * as template from './TuneList.html';

import {Tune} from "../../score/Tune";
import {ABCParser} from "../../score/ABCParser-ABCjs";

import './tuneList.scss';
import Ajax from "../../util/Ajax";
import DisplayConfiguration from "../../util/DisplayConfiguration";
import {Length, Note} from "../../score/Note";
import {CollectionPointer} from "../collectionList/CollectionList";

export class TunePointer {
    url: string;
    name: string;

    constructor(name: string, url: string) {
        this.name = name;
        this.url = url;
    }
}

@Riot.template(template.toString())
export class TuneList extends Riot.Element {
    private currentTune: Tune;
    private currentTuneIndex = 0;
    private tunePointers: TunePointer[] = [];
    private tunes: Tune[] = [];
    private storage: Storage;

    constructor() {
        super();
        this.storage = StorageService.getInstance();
    }

    mounted() {
        PubSub.subscribe('collectionChanged', (collection: CollectionPointer) => this.listChanged(collection) );
        PubSub.subscribe('tempoChanged', (tempo: number) => this.storage.storeTuneTempo( this.tunePointers[this.currentTuneIndex], tempo ) );
    }

    private listChanged(collectionPointer: CollectionPointer): void {
        this.getTuneList( collectionPointer.url, (tp) => {
            this.tunePointers = tp;

            const tune = this.storage.getCurrentTune();
            let index = this.indexOf(this.tunePointers, tune);
            if( index < 0 ) index = 0;

            this.changeTune( this.tunePointers[index].url, index );
            this.update();
        } );
    }

    private tuneSelected(e): void {
        let index = parseInt(e.target.value);
        let pointer = this.tunePointers[index];
        this.changeTune(pointer.url, index);
    }

    private changeTune(url: string, index: number) {
        PubSub.publish('tuneChanged', this.tunePointers[index] );
        this.storage.setCurrentTune( this.tunePointers[index] );

        Ajax.get( url, (abc: string) => {
            this.tunes[index] = new ABCParser().parse(abc);
            this.currentTune = this.tunes[index];
            this.currentTuneIndex = index;
            TuneList.addLeadIn( this.currentTune );

            const tempo = this.storage.getTuneTempo( this.tunePointers[this.currentTuneIndex] );
            if( tempo ) {
                this.currentTune.tempo = tempo;
            }

            window.setTimeout( () => PubSub.publish("tuneChosen", this.currentTune), 100 );
            this.update();
        });
    }

    private getTuneList( url: string, callback: (_:TunePointer[]) => void ) {
        Ajax.get( url, (csv: string) => {
            let tunePointers = [];
            let lines = csv.split('\n');
            for( let line of lines ) {
                let parts = line.split(',');
                if( parts[0].trim() ) {
                    tunePointers.push( new TunePointer( parts[0], parts[1] ) );
                }
            }
            callback( tunePointers );
        });
    }

    private static addLeadIn(currentTune: Tune) {
        let firstBarIndex = 0;
        let firstBarPosition = 0;

        // TODO: A tune with no bars?
        while( !currentTune.notes[firstBarIndex].isBarLine ) {
            firstBarPosition += currentTune.notes[firstBarIndex].length.value;
            firstBarIndex++;
        }

        const firstBarLine = currentTune.notes[firstBarIndex];
        const barLengthInVals = DisplayConfiguration.valuesPerBeat * currentTune.beatsPerBar;

        // The first bar line we found was actually the second bar line (i.e. tune had no lead-in)
        if( firstBarPosition == barLengthInVals ) {
            // Add an empty lead-in bar
            const leadInBar: Note[] = [
                new Note( undefined, new Length(0), true ),
                new Note( undefined, new Length(barLengthInVals), false, undefined, true),
                new Note( undefined, new Length(0), true )
            ];
            currentTune.notes = leadInBar.concat( currentTune.notes );
        } else {
            // There was a lead in bar, so add as much rest before to create a complete bar.
            const leadInBar: Note[] = [
                new Note( undefined, new Length(0), true ),
                new Note( undefined, new Length(barLengthInVals - firstBarPosition), false, undefined, true)
            ];
            currentTune.notes = leadInBar.concat( currentTune.notes );
        }

        // Recalculate tune metrics
        currentTune.finalise();
    }

    private indexOf(arr, obj) {
        if( obj === undefined ) {
            return -1;
        }
        let keys = Object.keys(obj);
        return arr.findIndex((cur) => keys.every((key) => obj[key] === cur[key]));
    }
}
