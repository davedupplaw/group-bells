import * as Riot from 'riot-typescript';
import PubSub from "../../util/PubSub";
import {StorageService} from "../../util/StorageService";
import {Storage} from "../../util/Storage";

import * as template from './CollectionList.html';
import './collectionList.scss';

export class CollectionPointer {
    url: string;
    name: string;

    constructor(name: string, url: string) {
        this.name = name;
        this.url = url;
    }
}

@Riot.template(template.toString())
export class CollectionList extends Riot.Element {
    private currentCollection: CollectionPointer;
    private collectionPointers: CollectionPointer[] = [
        new CollectionPointer('Christmas Carols', 'lists/carols/carols.list'),
        new CollectionPointer('Common Irish Session Tunes', 'lists/irish/common-irish-session.list'),
        new CollectionPointer('Tests', 'lists/tests/tests.list')
    ];
    private storage: Storage;

    constructor() {
        super();
        this.storage = StorageService.getInstance();
    }

    mounted() {
        const collection: CollectionPointer = this.storage.getCurrentCollection();
        const index = CollectionList.indexOf(this.collectionPointers, collection);

        // The timeout is to give time for the other elements to mount
        window.setTimeout(() => {
            this.collectionSelected({target: {value: index < 1 ? 0 : index}});
        }, 200);
    }

    private collectionSelected(e) {
        let index = parseInt(e.target.value);
        this.currentCollection = this.collectionPointers[index];
        this.storage.setCurrentCollection( this.currentCollection );
        PubSub.publish('collectionChanged', this.currentCollection);
        this.update();
    }

    private static indexOf(arr, obj) {
        if( obj === undefined ) {
            return -1;
        }
        let keys = Object.keys(obj);
        return arr.findIndex((cur) => keys.every((key) => obj[key] === cur[key]));
    }
}
