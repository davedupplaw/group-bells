import PubSub from "./PubSub";
import DisplayConfiguration from "./DisplayConfiguration";

class TimingController {
    /* position in pixels */
    private position: number = 0;

    /* current tempo */
    private tempoBPM: number = 100;

    /* whether or not the timer is running */
    private running: boolean = false;

    /* How long the timer's been running */
    private runtime: number = 0;

    /* The position of the playhead when the tune started */
    private playheadOffet: number = 0;

    /* The time the timer started playing the tune (provided by requestAnimationFrame() */
    private starttime: number;

    /* The current animation request from the window */
    private animationRequestHandle: number;

    constructor() {
        PubSub.subscribe('play', () => this.resetAndStart() )
    }

    resetAndStart() {
        this.reset();
        this.start();
    }

    reset() {
        this.position = 0;
        this.playheadOffet = 0;
        this.firePositionChanged();
        this.fireTransportReset();
    }

    start() {
        this.starttime = 0;
        this.running = true;
        this.setTimeoutForNextMove();
        this.fireTransportStarted();
    }

    pause() {
        window.cancelAnimationFrame( this.animationRequestHandle );
        this.playheadOffet = this.position;
        this.running = false;
        this.fireTransportPaused();
    }

    pauseAndReset() {
        this.pause();
        this.reset();
    }

    setTempo(tempo: number) {
        this.tempoBPM = tempo;
    }

    private setTimeoutForNextMove() {
        this.animationRequestHandle = window.requestAnimationFrame( (ts: number) => this.shuttleTransport(ts) );
    }

    private shuttleTransport( timestamp : number ) {
        this.starttime = this.starttime || timestamp;
        this.runtime = timestamp - this.starttime;
        this.position = this.runtime * this.tempoBPM * DisplayConfiguration.valuesPerBeat / 60000.0 + this.playheadOffet;
        this.firePositionChanged();

        if( this.running ) {
            this.setTimeoutForNextMove();
        }
    }

    private firePositionChanged() {
        PubSub.publish('transportMoved', this.position );
    }

    toggle() {
        if( this.running ) {
            this.pause();
        } else {
            this.start();
        }
    }

    private fireTransportStarted() {
        PubSub.publish('transportStarted', this.position );
    }

    private fireTransportPaused() {
        PubSub.publish( 'transportPaused', this.position );
    }

    private fireTransportReset() {
        PubSub.publish('transportReset', this.position );
    }
}

export default new TimingController();