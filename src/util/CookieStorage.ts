import * as Cookies from 'js-cookie';

import {Storage} from "./Storage";
import {CollectionPointer} from "../ui/collectionList/CollectionList";
import {TunePointer} from "../ui/tuneList/TuneList";

class TuneTempo {
    tuneUrl: string;
    tempo: number;

    constructor(tempo: number, tune?: TunePointer, tuneUrl?: string) {
        this.tuneUrl = tune ? tune.url : tuneUrl;
        this.tempo = tempo;
    }
}

export class CookieStorage implements Storage {
    getCurrentCollection(): CollectionPointer {
        const collectionJson = Cookies.get('collection');
        if( collectionJson ) {
            return JSON.parse(collectionJson);
        }
        return undefined;
    }

    setCurrentCollection(collection: CollectionPointer): void {
        Cookies.set('collection', collection);
    }

    getCurrentTune(): TunePointer {
        let tuneJson = Cookies.get('tune');
        if( tuneJson ) {
            return JSON.parse(tuneJson);
        }
        return undefined;
    }

    setCurrentTune(tune: TunePointer): void {
        Cookies.set('tune', tune);
    }

    storeTuneTempo(tune: TunePointer, tempo: number): void {
        const tuneTempo = new TuneTempo(tempo, tune);
        let tuneTempos = CookieStorage.getAllTuneTempos();

        const index = CookieStorage.indexOf( tuneTempos, tuneTempo, 'tuneUrl' );
        if( index >= 0 ) {
            tuneTempos.splice(index,1);
        }

        tuneTempos.push( tuneTempo );

        Cookies.set('customTempos', tuneTempos );
    }

    getTuneTempo(tune: TunePointer) {
        const tuneTempos = CookieStorage.getAllTuneTempos();
        const index = CookieStorage.indexOf(tuneTempos, new TuneTempo(0, undefined, tune.url), 'tuneUrl');
        if( index >= 0 ) {
            return tuneTempos[index].tempo;
        }
        return undefined;
    }

    private static getAllTuneTempos(): TuneTempo[] {
        const cookieValue = Cookies.get('customTempos');

        if( cookieValue ) {
            return JSON.parse( cookieValue );
        }

        return [];
    }

    private static indexOf(arr, obj, key?): number {
        let keys = Object.keys(obj);

        if( key ) {
            return arr.findIndex((cur) => cur[key] === obj[key]);
        }

        return arr.findIndex((cur) => keys.every((key) => obj[key] === cur[key]));
    }
}