export class Version {
	public static versionNumber = "$VERSION";

	get version() {
		return Version.versionNumber;
	}
}
