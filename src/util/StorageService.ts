import {CookieStorage} from "./CookieStorage";
import {Storage} from "./Storage";

export class StorageService {
    private static _instance: Storage;

    public static getInstance() : Storage {
        if( !StorageService._instance ) {
            StorageService._instance = new CookieStorage();
        }
        return StorageService._instance;
    }
}