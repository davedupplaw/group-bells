class DisplayConfiguration {
    public valuesPerBeat: number = 24;
    public positionMarkerAt: number = 200;  // Also need to change in tuneDisplay.scss
    public beatsPerPixel : number = 0.01;
}

export default new DisplayConfiguration();