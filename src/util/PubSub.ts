export class PubSub {
    private debug: boolean = false;
    private listeners: {[index:string]:{(any):void}[];};

    constructor() {
        this.listeners = {};
    }

    subscribe( topic: string, listener: (any) => void ): void {
        if( !this.listeners[topic] ) {
            this.listeners[topic] = [];
        }

        if( this.debug ) { console.log( `Adding listener to ${topic}`); }
        this.listeners[topic].push( listener );
    }

    publish( topic: string, data: any ): void {
        if( this.debug ) {
            console.log( `Publishing to ${topic}: ${JSON.stringify(data)}`);
            console.log( `There are ${this.listeners[topic]? this.listeners[topic].length : 0} listeners for that topic` );
        }

        if( this.listeners[topic] ) {
            this.listeners[topic].forEach( (l) => l(data) );
        }
    }
}

export default new PubSub();