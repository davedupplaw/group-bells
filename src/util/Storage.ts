import {CollectionPointer} from "../ui/collectionList/CollectionList";
import {TunePointer} from "../ui/tuneList/TuneList";

export interface Storage {
    setCurrentCollection(collection: CollectionPointer);
    getCurrentCollection(): CollectionPointer;

    setCurrentTune(tune: TunePointer);
    getCurrentTune(): TunePointer;

    storeTuneTempo(tune: TunePointer, tempo: number);
    getTuneTempo(tune: TunePointer);
}