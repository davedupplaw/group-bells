import * as riot from "riot/riot+compiler";
import {BellsApp} from './ui/BellsApp';

// avoid tree-shaking this out
let bells: BellsApp = new BellsApp();

riot.mount('bells-app');