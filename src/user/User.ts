export class User {
    private _name;
    private _email;
    private _identifier;

    get identifier() {
        return this._identifier;
    }

    set identifier(value) {
        this._identifier = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}