import {Tune} from "./Tune";
import {Length, Note, Pitch} from "./Note";
import DisplayConfiguration from "../util/DisplayConfiguration";

export class ABCParser {
    private noteMap: any = { "C": 60, "D": 62, "E": 64, "F": 65, "G": 67, "A": 69, "B": 71, "c": 72, "d": 74, "e": 76, "f": 77, "g": 79, "a": 81, "b": 83 };
    private keyMap: any = {
        "C":     {},
        "Am":    {},
        "Dmix":  {},
        "G":     {65: +1, 77: +1},
        "Em":    {65: +1, 77: +1},
        "Edor":  {65: +1, 77: +1},
        "Amix":  {65: +1, 77: +1},
        "D":     {65: +1, 77: +1, 60: +1, 72: +1},
        "A":     {65: +1, 77: +1, 60: +1, 72: +1, 67: +1, 79: +1},
        "E":     {65: +1, 77: +1, 60: +1, 72: +1, 67: +1, 79: +1, 62: +1, 74: +1},
        "B":     {65: +1, 77: +1, 60: +1, 72: +1, 67: +1, 79: +1, 62: +1, 74: +1, 69: +1, 81: +1},
        "F#":    {65: +1, 77: +1, 60: +1, 72: +1, 67: +1, 79: +1, 62: +1, 74: +1, 69: +1, 81: +1, 64: +1, 76: +1},
        "Gb":    {65: +1, 77: +1, 60: +1, 72: +1, 67: +1, 79: +1, 62: +1, 74: +1, 69: +1, 81: +1, 64: +1, 76: +1},
        "C#":    {71: -1, 83: -1, 64: -1, 76: -1, 69: -1, 81: -1, 62: -1, 74: -1, 67: -1, 79: -1},
        "Db":    {71: -1, 83: -1, 64: -1, 76: -1, 69: -1, 81: -1, 62: -1, 74: -1, 67: -1, 79: -1},
        "G#":    {71: -1, 83: -1, 64: -1, 76: -1, 69: -1, 81: -1, 62: -1, 74: -1},
        "Ab":    {71: -1, 83: -1, 64: -1, 76: -1, 69: -1, 81: -1, 62: -1, 74: -1},
        "D#":    {71: -1, 83: -1, 64: -1, 76: -1, 69: -1, 81: -1},
        "Eb":    {71: -1, 83: -1, 64: -1, 76: -1, 69: -1, 81: -1},
        "A#":    {71: -1, 83: -1, 64: -1, 76: -1},
        "Bb":    {71: -1, 83: -1, 64: -1, 76: -1},
        "F":     {71: -1, 83: -1},
    };

    parse( abc: string ) : Tune {
        let startSeen: boolean = false;
        let tune: Tune;
        let lines: string[] = abc.split('\n');
        let scalar = 1;
        let music = "";

        for( let line of lines ) {
            let trimmedLine = line.trim();

            switch( line[0] ) {
                case 'X':
                    startSeen = true;
                    break;
                case 'T' :
                    tune = new Tune( line.split(':')[1].trim() );
                    break;
                case 'K':
                    tune.key = line.split(':')[1].trim();
                    break;
                case 'L':
                    let lineValue = line.split(':')[1].trim();
                    let given = parseFloat(lineValue.split('/')[0] ) / parseFloat(lineValue.split('/')[1] );
                    scalar = given / 0.25;  // 1/4 (0.25) is default scale
                    break;
                case 'M':
                    tune.beatsPerBar = parseInt(line.split(':')[1].trim().split('/')[0]);
                    break;
                case 'Q':
                    tune.tempo = parseInt(line.split(':')[1].trim());
                    break;
                // ignore these headers
                case 'R': break;
                case 'S': break;
                case 'Z': break;
                default:
                    music += line;
            }
        }

        this.parseMusic(music, tune, scalar);
        tune.finalise();
        return tune;
    }

    private parseMusic(line: string, tune: Tune, scalar) {
        const noteLength = DisplayConfiguration.valuesPerBeat * scalar;

        let repeatCache : Note[];
        let note: Note;

        let nextNoteAccidental = 0;
        let naturaliseNextNote = false;
        let nextNoteLengthMultiplier = 1;

        console.log( line );
        for( let i = 0; i < line.length; i++ ) {
            let char = line[i];

            if( char === '|' && line[i+1] === ':' ) {
                repeatCache = [];
                i++;
            } else if( char === ':' && line[i+1] === '|' ) {
                for( let note of repeatCache ) {
                    tune.addNote( note );
                }
                i++;
            } else if( char === '|' ) {
                note = new Note( undefined, new Length(0), true );
                tune.addNote( note );
            } else if( char === 'z' ) {
                note = new Note( undefined, new Length(noteLength), false, undefined, true );
                tune.addNote( note );
            } else if( /[ABCDEFGabcdefg]/.test(char) ) {
                let noteNumber = this.noteMap[char];
                let offset = this.keyMap[tune.key][noteNumber] || 0;

                let noteValue = noteNumber + offset + nextNoteAccidental;
                if( naturaliseNextNote ) {
                    noteValue = noteNumber;
                }

                note = new Note( new Pitch(noteValue), new Length(noteLength * nextNoteLengthMultiplier) );
                tune.addNote( note );

                nextNoteAccidental = 0;
                naturaliseNextNote = false;
                nextNoteLengthMultiplier = 1;
            } else if( char === '^' ) {
                nextNoteAccidental++;
            } else if( char === '_' ) {
                nextNoteAccidental--;
            } else if( char === '=' ) {
                naturaliseNextNote = true;
            } else if( char === '/' ) {
                if( /[0-9]/.test(line[i+1]) ) {
                    note.length.value /= line[i+1].charCodeAt(0)-48;
                    i++;
                } else {
                    note.length.value /= 2;
                }
            } else if( char === '>' ) {
                note.length.value *= 1.5;
                nextNoteLengthMultiplier = 0.5;
            } else if( char === '<' ) {
                note.length.value /= 2;
                nextNoteLengthMultiplier = 1.5;
            } else if( /[2345678]/.test(char) ) {
                note.length.value = noteLength * parseInt(char);
            } else if( char === ',' ) {
                note.pitch.value -= 12;
            } else if( char === '"' ) {
                let str = '';
                i++;
                do {
                    str += line[i];
                    i++;
                } while (line[i] !== '"');
                let comment = new Note( undefined, new Length(0), false, str );
                tune.addNote( comment );
            }
        }
    }
}