export class Pitch {
    private pitches: string[] = ["C", "C#", "D", "Eb", "E", "F", "F#", "G", "G#", "A", "Bb", "B" ];

    public value: number;
    public name: string;

    constructor(value: number) {
        this.value = value;
        this.name = this.pitches[value%12];
    }
}

export class Length {
    public value: number;

    constructor(value: number) {
        this.value = value;
    }
}

export class Note {
    private _pitch: Pitch;
    private _length: Length;
    private _isBarLine: boolean = false;
    private _isComment: boolean = false;
    private _comment: string;
    private _isRest: boolean;

    constructor( pitch: Pitch = undefined, length: Length = undefined, isBarLine: boolean = false, comment: string = undefined, isRest: boolean = false ) {
        this._pitch = pitch;
        this._length = length;
        this._isBarLine = isBarLine;
        this._isRest = isRest;
        if( comment ) {
            this._isComment = true;
            this._comment = comment;
        }
    }

    get length(): Length {
        return this._length;
    }

    get pitch(): Pitch {
        return this._pitch;
    }

    get isBarLine(): boolean {
        return this._isBarLine;
    }

    get isComment(): boolean {
        return this._isComment;
    }

    get isRest(): boolean {
        return this._isRest;
    }

    get comment(): string {
        return this._comment;
    }
}