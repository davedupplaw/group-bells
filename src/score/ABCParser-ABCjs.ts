import {Tune} from './Tune';
import * as abcjs from 'abcjs/test';
import {Length, Note, Pitch} from './Note';
import DisplayConfiguration from '../util/DisplayConfiguration';

export class ABCParser {
	parse(abc: string): Tune {
		console.log(abc);

		const tuneBook = new abcjs.TuneBook(abc);
		const abcParser = new abcjs.parse.Parse();
		const item = tuneBook.tunes[0];
		const tune = new Tune(item.title);
		tune.tempo = item.tempo || 100;

			abcParser.parse(item.abc);
		const parsedTune = abcParser.getTune();
		const sequence = abcjs.midi.sequence(parsedTune);
		const midi = abcjs.midi.flatten(sequence);
		console.log(parsedTune);
		tune.beatsPerBar = parsedTune.getBeatsPerMeasure();
		const beatLength = parsedTune.getBeatLength();
		const noteLength = DisplayConfiguration.valuesPerBeat / beatLength;

		let repeatMarker = tune.notes.length;
		let beatsThisBar = 0;
		let repeatCount = 1;
		let repeatEndings = [];
		repeatEndings[0] = 0;
		parsedTune.lines.forEach(line => {
			line.staff[0].voices[0].forEach(event => {
				switch (event.el_type) {
					case 'note':
						if (event.midiPitches) {
							if (event.chord && event.chord[0] && event.chord[0].name) {
								const comment = new Note( undefined, new Length(0), false, event.chord[0].name);
								tune.addNote( comment );
							}

							for(let pitchIndex = 0; pitchIndex < event.midiPitches.length; pitchIndex++) {
								const pitch = event.midiPitches[pitchIndex].pitch;
								const note = new Note(new Pitch(pitch), new Length(
									pitchIndex !== event.midiPitches.length - 1 ? 0 : event.duration * noteLength));
								tune.addNote(note);
							}

						} else if (event.rest.type === 'rest') {
							const note = new Note(undefined, new Length(event.duration * noteLength), false, undefined, true);
							tune.addNote(note);
						}
						beatsThisBar += event.duration;
						break;
					case 'bar': {
						switch (event.type) {
							case "bar_left_repeat":
								repeatMarker = tune.notes.length;
								repeatCount = 1;
								repeatEndings = [];
								repeatEndings[0] = repeatMarker;
								break;
							case "bar_right_repeat":
								// Check whether this last bar is full; if so add a bar line
								if (beatsThisBar === tune.beatsPerBar * beatLength) {
									const note = new Note(undefined, new Length(0), true);
									tune.addNote(note);
								}

								// Copy all the data from the repeat marker to where the endings started,
								// OR to where we currently are
								let length = repeatEndings[1] || tune.notes.length;
								for(let i = repeatMarker; i < length; i++) {
									tune.addNote(tune.notes[i]);
								}

								// Reset the repeat marker
								repeatMarker = tune.notes.length + 1;
								break;
							default:
								const note = new Note(undefined, new Length(0), true);
								tune.addNote(note);

								if (event.startEnding) {
									repeatEndings[event.startEnding] = tune.notes.length + 1;
								}
						}
						beatsThisBar = 0;
						break;
					}
					default:
						break;
				}
			});
		});

		console.log(tune);
		tune.finalise();
		return tune;
	}
}
