import {Note} from "./Note";

export class Tune {
    private readonly _title: string;

    private numberOfNotes: number = 0;
    private lengthInValues = 0;
    private _notes: Note[] = [];
    private _key: string;
    private _beatsPerBar = 4;
    private _tempo = 100;

    constructor( title: string ) {
        this._title = title;
    }

    addNote(note: Note) {
        this._notes.push(note);
        this.numberOfNotes++;
    }

    get title(): string {
        return this._title;
    }

    get key(): string {
        return this._key;
    }

    set key(value: string) {
        this._key = value;
    }

    get notes(): Note[] {
        return this._notes;
    }

    set notes( notes: Note[] ) {
        this._notes = notes;
    }

    get tuneLength(): number {
        return this.lengthInValues;
    }

    get beatsPerBar(): number {
        return this._beatsPerBar;
    }

    set beatsPerBar(value: number) {
        this._beatsPerBar = value;
    }

    get tempo(): number {
        return this._tempo;
    }

    set tempo(value: number) {
        this._tempo = value;
    }

    finalise() {
        this.lengthInValues = 0;
        for( let note of this.notes ) {
            this.lengthInValues += note.length.value;
        }
    }
}
