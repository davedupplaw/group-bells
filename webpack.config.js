const BabiliPlugin = require('babili-webpack-plugin');
const ExtractTestPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTestPlugin('default.css');

module.exports = {
    entry: "./src/index.tsx",
    output: {
        path: __dirname + "/dist",
        filename: "bells.js",
        library: 'bells',
        libraryTarget: 'umd',
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx', '.svg', '.html'],
        modules: [__dirname + '/src', 'node_modules']
    },
    plugins: [
        // new BabiliPlugin(),
        extractCSS
    ],
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.css$/,
                use: extractCSS.extract({ fallback: 'style-loader', use: 'css-loader?minimize=true' })
            }, {
                test: /\.scss$/,
                use: extractCSS.extract({ fallback: 'style-loader', use: ['css-loader?minimize=true', 'sass-loader'] })
            }, {
                test: /\.svg$/,
                loader: 'raw-loader'
            }, {
                test: /\.html$/,
                loader: 'raw-loader'
            }, {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            }, {
                test: /\.png$/,
                loader: 'file-loader'
            }, {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            }
        ]
    },
    externals: {
        riot: 'riot'
    }
};
