export class Version {
	public static versionNumber = "wibble";

	get version() {
		return Version.versionNumber;
	}
}
