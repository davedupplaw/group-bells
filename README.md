# Group Hand Bells

This is an app for helping a group of people play a set of toy handbells.

It's a RiotJS app, written in Typescript with SCSS styling.

Find out more about it, including a video, [on my blog](http://blog.dupplaw.me.uk/articles/2017-12/christmas-handbell-app-for-groups).

## Running

Run these commands.  The second two need to be in separate shells. tmux FTW. 

```
npm install
node_modules/.bin/webpack --watch
node_modules/.bin/live-server
```

## Acknowledgements

Uses a bell icon by Daniel Bruce from [FlatIcon](http://flaticon.com).